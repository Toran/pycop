#!/usr/bin/python
import os
import re
import sys
import time
import commands
from datetime import date
import timeit
import ConfigParser
import io
import funciones
import config
import rsync/backup

print "Iniciando Backup " + str(date.today()) 



#Espacio Ocupado Servidor
ExecCommpleto= "ssh " + RemoteUser + '@' + RemoteHost +" du -s " + Completo + " | cut -d '/' -f 1"
[status,intCompleto]=commands.getstatusoutput(ExecCommpleto)
ExecDiferencial= "ssh " + RemoteUser + '@' + RemoteHost +" du -s " + Diferencial + " | cut -d '/' -f 1"
[status,intDiferencial]=commands.getstatusoutput(ExecDiferencial)

#Suma de Espacios
SCompleto=float(float(intCompleto)/1024/1024) #Lo convertimos en mb
SDiferencial=float(float(intDiferencial)/1024/1024) #Lo convertimos en mb

STotal=float(SDiferencial+SCompleto)


 
#Mostramos los datos finales
CadenaFinal=str(date.today()) + "  " + Host + "  Transferidos " + "{0:.2f}".format(MBytesEnviados) + " Mbytes  TiempoTotal " +  "{0:.2f}".format(float(tiempoTotal)/60)+ " Minutos   Completo " + "{0:.2f}".format(SCompleto) +" GiB  Diferencial "  + "{0:.2f}".format(SDiferencial)   + " GiB Total " + "{0:.2f}".format(STotal)  + " GiB"
print CadenaFinal

ImprimirDatos= "ssh " + RemoteUser + '@' + RemoteHost +" \"echo '" + CadenaFinal  + "' >> "+LogServer + "\""
[status,intCompleto]=commands.getstatusoutput(ImprimirDatos)



