import ConfigParser,os,inspect,sys

from datetime import date

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
for folder in ['rsync','server','bbdd']:
	sys.path.insert(0, folder)
	

#Config Parser
config = ConfigParser.ConfigParser()
config.read('/etc/proyectr/config')

Version="0.01"


#Creamos el diccionario de la configuracion
DataConfig={"":""}

#Backup de Mysql
for Attribute in ['mysql_backup','mysql_user' ,'mysql_pass']:
	DataConfig[Attribute]=config.get('Mysql', Attribute, 0)

#Backup Psql
for Attribute in ['psql_backup','psql_user' ,'psql_pass']:
	DataConfig[Attribute]=config.get('PostgreSQL', Attribute, 0)

#Local Config
for Attribute in ['RemoteUser','Folder' ,'Host']:
	DataConfig[Attribute]=config.get('Client', Attribute, 0)

DataConfig['Folder'] = DataConfig['Folder'].split(',')    	


#Server Config
for Attribute in ['RemoteHost','RemoteDir' ,'LogServidor']:
	DataConfig[Attribute]=config.get('Server', Attribute, 0)

DataConfig['Differential']= DataConfig['RemoteDir']+'modificado/backup_'+ str(date.today())+'/'
DataConfig['FullBackup']= DataConfig['RemoteDir']+'completo/'



