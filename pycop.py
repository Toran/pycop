#!/usr/bin/env python
import os,re,sys,time,commands,inspect,timeit,ConfigParser,io,datetime
#Apps imports
import config	
import function

"Rsync Backup"

__author__ = "Jonatan Menendez Canal <jonatanmenendez@gmail.com>"
__copyright__ = "Copyright (C) 2012 Jonatan Menendez Canal"
__license__ = "LGPL 3.0"


print "Arrancamos PyCop v" + config.Version + " \t" + str(datetime.datetime.today())


#Comprueba Mysql
function.bbdd.mysql.Backup(config.DataConfig)

#Comprueba PSQL
function.bbdd.psql.Backup(config.DataConfig)

#Algoritmo para Sincronizar 
function.rsync.Backup(config.DataConfig)
 
#Mostramos los datos finales
#CadenaFinal=str(date.today()) + "  " + Host + "  Transferidos " + "{0:.2f}".format(MBytesEnviados) + " Mbytes  TiempoTotal " +  "{0:.2f}".format(float(tiempoTotal)/60)+ " Minutos   Completo " + "{0:.2f}".format(SCompleto) +" GiB  Diferencial "  + "{0:.2f}".format(SDiferencial)   + " GiB Total " + "{0:.2f}".format(STotal)  + " GiB"
#print CadenaFinal

#ImprimirDatos= "ssh " + RemoteUser + '@' + RemoteHost +" \"echo '" + CadenaFinal  + "' >> "+LogServer + "\""
#status,intCompleto]=commands.getstatusoutput(ImprimirDatos)



