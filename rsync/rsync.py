#!/usr/bin/env python
import time
import commands
import os

"Rsync Backup"

__author__ = "Jonatan Menendez Canal <jonatanmenendez@gmail.com>"
__copyright__ = "Copyright (C) 2012 Jonatan Menendez Canal"
__license__ = "LGPL 3.0"
__version__ = "1.02c"

def Backup(DataConfig):

	BytesEnviados=0
	inicio = time.time()

	print "Iniciando Sincronizacion"
	#Sincronizamos
	for Sync in DataConfig['Folder']:
	    SyncCmd = 'rsync   -avb --delete --backup-dir='+ DataConfig['Differential']  +' '+ Sync + ' ' + DataConfig['RemoteUser'] + '@' + DataConfig['RemoteHost']+ ':' + DataConfig['FullBackup'] + " > salida.log"
	    [status,output]=commands.getstatusoutput(SyncCmd)
	    Read2='grep sent salida.log| tail -n 1' 
	    [status,Read]=commands.getstatusoutput(Read2)
	    BytesEnviados=BytesEnviados+ int(Read.split(' ')[1])
	   
	print "Finalizando Sincronizacion"
	#Fin de los bucles, nos preparamos para enviar Mail

	MBytesEnviados=float(float(BytesEnviados)/1024/1024)

	fin = time.time()
	tiempoTotal=fin-inicio
