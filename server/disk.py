import os
import commands


#Espacio Ocupado Servidor
ExecCommpleto= "ssh " + RemoteUser + '@' + RemoteHost +" du -s " + Completo + " | cut -d '/' -f 1"
[status,intCompleto]=commands.getstatusoutput(ExecCommpleto)
ExecDiferencial= "ssh " + RemoteUser + '@' + RemoteHost +" du -s " + Diferencial + " | cut -d '/' -f 1"
[status,intDiferencial]=commands.getstatusoutput(ExecDiferencial)

#Suma de Espacios
SCompleto=float(float(intCompleto)/1024/1024) #Lo convertimos en mb
SDiferencial=float(float(intDiferencial)/1024/1024) #Lo convertimos en mb

STotal=float(SDiferencial+SCompleto)
